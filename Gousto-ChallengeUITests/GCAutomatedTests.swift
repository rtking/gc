//
//  Gousto_ChallengeUITests.swift
//  Gousto-ChallengeUITests
//
//  Created by Ryan on 12/04/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import XCTest

class GCAutomatedTests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()
    }
    
    func testCheckAllInFilter() {
        let app = XCUIApplication()
        
        app.navigationBars["Products"].buttons["Filter"].tap()
        app.toolbars.buttons["Check All"].tap()
        app.buttons["Back"].tap()
        
        // We know we should have all products in our table
        XCTAssertEqual(app.tables.cells.count, 184)
    }
    
    func testUncheckAllInFilter() {
        let app = XCUIApplication()
        
        app.navigationBars["Products"].buttons["Filter"].tap()
        app.toolbars.buttons["Uncheck All"].tap()
        app.buttons["Back"].tap()
        
        // We know we should have no products in our table
        XCTAssertEqual(app.tables.cells.count, 0)
    }
    
    func testMostPopularInFiltered() {
        let app = XCUIApplication()
        
        // Check that we have one table view
        XCTAssertEqual(app.tables.count, 1)
        
        app.navigationBars["Products"].buttons["Filter"].tap()
        app.toolbars.buttons["Uncheck All"].tap()
        app.tables.cells.containing(.staticText, identifier:"Most Popular").children(matching: .switch).matching(identifier: "Most Popular").element(boundBy: 0).tap()
        app.buttons["Back"].tap()
        
        // We know we should have only one valentines day option in the tableview
        XCTAssertEqual(app.tables.cells.count, 29)
    }
}
