### What is this repository for? ###

* Quick summary: This is a Gousto mobile coding challenge for Ryan King
* Version: 1.0

### Notes ###

* I have written a few unit tests for my networking, parsing and filtering logic.
* I have created a few automated tests to ensure the application works as expected.
* I appreciate that using Core Data would have been a better solution. I don't have a lot of experience with Core Data and given the limited time available I decided to settle using NSCoding and NSUserdefaults. I fully understand that this is not a great persistence layer.
* The application can be used comfortably offline (assuming the data has previously been retrieved).
* The application assumes that products can change, therefore retrieving products at every opportunity.
* The application assumes that categories do not change, not retrieving categories again.

### TODO's ###

* Change the persistence layer to use Core Data instead of NSUserdefaults.
* Add more data to the details page, such as age restriction etc.
* Add some animations and custom UI elements (such as a nicer UISwitch in the Filter Page)