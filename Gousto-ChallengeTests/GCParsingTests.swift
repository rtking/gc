//
//  GCParsingTests.swift
//  Gousto-Challenge
//
//  Created by Ryan on 12/04/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import XCTest
@testable import Gousto

class GCParsingTests: XCTestCase {
    
    var categoriesJSONData : Data!
    var productsJSONData : Data!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // Setup the JSON from test files
        self.categoriesJSONData = getDataFromLocalJSONFile(resourceName: "allcategories")
        self.productsJSONData = getDataFromLocalJSONFile(resourceName: "allproducts")
    }
    
    // Function gets data from local JSON file, given a particular resource name
    func getDataFromLocalJSONFile(resourceName : String) -> Data {
        var data = Data()
        // Setup the JSON test file
        if let path = Bundle.main.path(forResource: resourceName, ofType: "json") {
            do {
                // Turn the JSON into data
                data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
            } catch let error {
                // Something has gone wrong with our deserialization, print the error out
                print(error.localizedDescription)
            }
        } else {
            print("Invalid filename/path.")
        }
        return data
    }
    
    func testParsingAllCategories() {
        // Ensure that the test data is not nil
        if self.categoriesJSONData != nil {
            // Function returns a list of JSON dictionaries
            let jsonArray = GCJSONParser.getJSONArrayFromResponseData(jsonData: self.categoriesJSONData)
            XCTAssertEqual(jsonArray.count, 20)
            // Function returns a list of categories
            let categoriesArray = GCJSONParser.createCategoryArrayFromJSONArray(jsonArray: jsonArray)
            XCTAssertEqual(categoriesArray.count, 20)
        } else {
            print("JSON Data is nil, sorry")
        }
    }
    
    func testParsingAllProducts() {
        // Ensure that the test data is not nil
        if self.productsJSONData != nil {
            // Function returns a list of JSON dictionaries
            let productsArray = GCJSONParser.getJSONArrayFromResponseData(jsonData: self.productsJSONData)
            XCTAssertEqual(productsArray.count, 214)
        } else {
            print("JSON Data is nil, sorry")
        }
    }
}
