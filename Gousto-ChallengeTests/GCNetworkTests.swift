//
//  File.swift
//  Gousto-Challenge
//
//  Created by Ryan on 12/04/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import XCTest
@testable import Gousto

class GCNetworkTests: XCTestCase {
    
    func testNetworkCallForCategories() {
        // Create expectation
        let expected = expectation(description: "API Call Complete")
        
        // Make network call for the categories
        GCHTTPClient.getAllCategories() { (data) in
            // Complete expectation
            expected.fulfill()
        }
        
        // Wait 10 seconds for expectation to be completed
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testNetworkCallForProductsWithCategoriesAndImagesIncluded() {
        // Create expectation
        let expected = expectation(description: "API Call Complete")
        
        // Make network call for the products, categories and images
        GCHTTPClient.getAllProductsWithCategoriesAndImagesIncluded(imageSize: 180) { (data) in
            expected.fulfill()
        }
        
        // Wait 10 seconds for expectation to be completed
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
