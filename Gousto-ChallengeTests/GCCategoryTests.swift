//
//  GCCategoryTests.swift
//  Gousto-Challenge
//
//  Created by Ryan on 18/04/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import XCTest
@testable import Gousto

class GCCategoryTests: XCTestCase {
    
    var categoriesJSONData : Data!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        self.categoriesJSONData = getDataFromLocalJSONFile(resourceName: "allcategories")
    }
    
    // Function gets data from local JSON file, given a particular resource name
    func getDataFromLocalJSONFile(resourceName : String) -> Data {
        var data = Data()
        // Setup the JSON test file
        if let path = Bundle.main.path(forResource: resourceName, ofType: "json") {
            do {
                // Turn the JSON into data
                data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
            } catch let error {
                // Something has gone wrong with our deserialization, print the error out
                print(error.localizedDescription)
            }
        } else {
            print("Invalid filename/path.")
        }
        return data
    }
    
    func testCategoryAvailableFilter() {
        // Function returns a list of JSON dictionaries
        let jsonArray = GCJSONParser.getJSONArrayFromResponseData(jsonData: self.categoriesJSONData)
        // Function returns a list of categories
        let categoriesArray = GCJSONParser.createCategoryArrayFromJSONArray(jsonArray: jsonArray)
        // Function filters out hidden categories
        let availableCategories = GCCategory.filterOnlyAvailableCategories(categeryArray: categoriesArray)
        XCTAssertEqual(availableCategories.count, 12)
    }
}
