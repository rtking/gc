//
//  GCDetailViewController.swift
//  Gousto-Challenge
//
//  Created by Ryan on 12/04/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import UIKit

class GCDetailViewController: UIViewController {
    
    // Outlets
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productDescriptionTextView: UITextView!
    @IBOutlet weak var productLimitLabel: UILabel!
    
    // Variables
    var product : GCProduct!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.layoutOutlets(product: self.product)
        self.updateImage()
    }
    
    func layoutOutlets(product : GCProduct) {
        // Set image original image
        if let imageString = product.imageString {
            productImageView.imageForURL(link: imageString)
        }
        
        // Set labels
        productTitleLabel.text = product.title
        productPriceLabel.text = "£\(product.price!)"
        productDescriptionTextView.text = product.descript
        productLimitLabel.text = "Limit : \(product.boxLimit!)"
    }
    
    func updateImage() {
        // Calculate required image size
        let imageSize = GCConstants.deviceScaleFactor*productImageView.frame.size.width
        
        GCHTTPClient.getProductForID(id: product.id, imageSize: Double(imageSize)) { (data) in
            let jsonArray = GCJSONParser.getJSONObjectFromResponseData(jsonData: data)
            if let updatedProduct = GCJSONParser.createProductObjectFromJSONArray(jsonObject: jsonArray) {
                // Reload our table in the main thread, as UIKit is not thread safe
                DispatchQueue.main.async {
                    // Reset the outlets
                    self.layoutOutlets(product: updatedProduct)
                }
            }
        }
    }
}
