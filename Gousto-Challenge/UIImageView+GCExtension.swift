//
//  UIImageView+GCExtension.swift
//  Gousto-Challenge
//
//  Created by Ryan on 18/04/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import UIKit

extension UIImageView {
    // Extension allows us to get an image for our imageview via a URL
    func imageForURL(url: URL) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            
                DispatchQueue.main.async() { () -> Void in
                    self.image = image
                }
        }.resume()
    }
    
    // Extension is a string friendly version of imageForURL
    func imageForURL(link: String) {
        guard let url = URL(string: link) else { return }
        imageForURL(url: url)
    }
}
