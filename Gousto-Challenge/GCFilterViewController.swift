//
//  GCFilterViewController.swift
//  Gousto-Challenge
//
//  Created by Ryan on 12/04/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import UIKit

class GCFilterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, GCFilterCellDelegate {
    // Outlets
    @IBOutlet weak var filterTableView : UITableView!
    
    // Variables
    var categories : [GCCategory]!
    
    // MARK: UIVIEW METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Instantiate our main category array
        self.categories = [GCCategory]()
        
        // Get our data to fill the main array
        self.getCategories()
    }
    
    @IBAction func dismissButtonPressed(_ sender: UIButton) {
        GCDatabaseHelper.saveCategories(newCategoryArray: self.categories)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func checkAllButtonPressed(_ sender: UIButton) {
        self.batchCheck(unCheckAll: false)

    }
    
    @IBAction func unCheckAllButtonPressed(_ sender: UIButton) {
        self.batchCheck(unCheckAll: true)
    }
    
    // MARK: DATA METHODS
    
    func getCategories() {
        // Check if we can retrieve the categories first, otherwise retrieve from the server
        if let categories = GCDatabaseHelper.retrieveCategories() {
            self.categories = categories
            self.categories = GCCategory.filterOnlyAvailableCategories(categeryArray: self.categories)
            self.reloadMyTableView()
        } else {
            GCHTTPClient.getAllCategories { (data) in
                let jsonArray = GCJSONParser.getJSONArrayFromResponseData(jsonData: data)
                self.categories = GCJSONParser.createCategoryArrayFromJSONArray(jsonArray: jsonArray)
                GCDatabaseHelper.saveCategories(newCategoryArray: self.categories)
                self.categories = GCCategory.filterOnlyAvailableCategories(categeryArray: self.categories)
                self.reloadMyTableView()
            }
        }
    }
    
    // MARK: FILTER CELL DELEGATE METHODS
    
    func filterChangedState(category : GCCategory, sender : UISwitch) {
        for object in self.categories {
            if (category.id == object.id) {
                object.filter = !sender.isOn
                break
            }
        }
    }
    
    // MARK: UITABLEVIEW METHODS
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(GCConstants.filterTableViewCellHeight)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Instantiate the custom cell
        let cell : GCFilterTableViewCell = self.filterTableView.dequeueReusableCell(withIdentifier: "filtercell") as! GCFilterTableViewCell
        // Connect delegate
        cell.delegate = self
        // Get the category out of the array and pass it to cell
        cell.category = self.categories[indexPath.row]
        // Set up the cell
        cell.setupCell()
        
        return cell
    }
    
    // MARK: HELPER METHODS
    
    private func batchCheck(unCheckAll : Bool) {
        self.categories.forEach { (object) in
            object.filter = unCheckAll
        }
        self.reloadMyTableView()
    }
    
    // Function reloads the TableView safely in the main thread
    func reloadMyTableView() {
        // Reload our table in the main thread, as UIKit is not thread safe
        DispatchQueue.main.async {
            self.filterTableView.reloadData()
        }
    }
}
