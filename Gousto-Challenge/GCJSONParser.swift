//
//  GCJSONParser.swift
//  Gousto-Challenge
//
//  Created by Ryan on 12/04/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import UIKit

class GCJSONParser {
    
    // MARK: ARRAY METHODS
    
    static func getJSONArrayFromResponseData(jsonData : Data?) -> [[String:Any]] {
        var dataArray = [[String:Any]]()
        
        do {
            let jsonResults = try JSONSerialization.jsonObject(with: jsonData!, options: []) as! [String:Any]
            if (jsonResults["data"] != nil) {
                dataArray = jsonResults["data"] as! [[String:Any]]
            }
        } catch let error {
            // Something has gone wrong with our deserialization, print the error out
            print(error.localizedDescription)
        }
        return dataArray
    }
    
    static func createCategoryArrayFromJSONArray(jsonArray : [[String:Any]]) -> [GCCategory] {
        var categoryArray = [GCCategory]()
        
        jsonArray.forEach { (category) in
            if let tempCat = GCCategory(json: category) {
                categoryArray.append(tempCat)
            }
        }
        
        return categoryArray
    }
    
    static func createProductArrayFromJSONArray(jsonArray : [[String:Any]]) -> [GCProduct] {
        var productArray = [GCProduct]()
        
        jsonArray.forEach { (product) in
            if let tempProd = GCProduct(json: product) {
                productArray.append(tempProd)
            }
        }
        
        return productArray
    }
    
    // MARK: OBJECT METHODS

    static func getJSONObjectFromResponseData(jsonData : Data?) -> [String:Any] {
        var dataObject = [String:Any]()
        
        do {
            let jsonResults = try JSONSerialization.jsonObject(with: jsonData!, options: []) as! [String:Any]
            if (jsonResults["data"] != nil) {
                dataObject = jsonResults["data"] as! [String:Any]
            }
        } catch let error {
            // Something has gone wrong with our deserialization, print the error out
            print(error.localizedDescription)
        }
        return dataObject
    }
    
    static func createProductObjectFromJSONArray(jsonObject : [String:Any]) -> GCProduct? {
        return GCProduct(json: jsonObject)
    }
}
