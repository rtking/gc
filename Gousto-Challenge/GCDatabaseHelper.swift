//
//  GCDatabaseHelper.swift
//  Gousto-Challenge
//
//  Created by Ryan on 18/04/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import UIKit

class GCDatabaseHelper {
    
    // MARK: CATEGORY METHODS
    
    static func saveCategories(newCategoryArray : [GCCategory]) {
        let archivedObject = archiveCategories(categories: newCategoryArray)
        UserDefaults.standard.set(archivedObject, forKey: GCConstants.allCategories)
        UserDefaults.standard.synchronize()
    }
    
    private static func archiveCategories(categories : [GCCategory]) -> NSData {
        return NSKeyedArchiver.archivedData(withRootObject: categories as NSArray) as NSData
    }
    
    static func retrieveCategories() -> [GCCategory]? {
        if let unarchivedObject = UserDefaults.standard.object(forKey: GCConstants.allCategories) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: unarchivedObject as Data) as? [GCCategory]
        }
        return nil
    }
    
    // MARK: PRODUCT METHODS

    static func saveProducts(newProductArray : [GCProduct]) {
        let archivedObject = archiveProducts(products: newProductArray)
        UserDefaults.standard.set(archivedObject, forKey: GCConstants.allProducts)
        UserDefaults.standard.synchronize()
    }
    
    private static func archiveProducts(products : [GCProduct]) -> NSData {
        return NSKeyedArchiver.archivedData(withRootObject: products as NSArray) as NSData
    }
    
    static func retrieveProducts() -> [GCProduct]? {
        if let unarchivedObject = UserDefaults.standard.object(forKey: GCConstants.allProducts) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: unarchivedObject as Data) as? [GCProduct]
        }
        return nil
    }
}
