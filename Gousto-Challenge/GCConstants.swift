//
//  GCConstants.swift
//  Gousto-Challenge
//
//  Created by Ryan on 12/04/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import UIKit
import Foundation

class GCConstants {
    // API Constants
    static let productAPIEndpoint = "https://api.gousto.co.uk/products/v2.0/products"
    static let categoryAPIEndpoint = "https://api.gousto.co.uk/products/v2.0/categories"
    
    // NSUserDefault Key Constants
    static let allCategories = "GCAllCategories"
    static let allProducts = "GCAllProducts"
    
    // UI Constants
    static let filterTableViewCellHeight = 64.0
    static let productTableViewCellHeight = 128.0
    static let productImageViewHeight = 120.0
    static let viewCornerRadius = 4.0
    static let deviceScaleFactor = UIScreen.main.scale

    // Calculate required image size, from device scale factor and standard size
    static let expectedImageSize = Double(GCConstants.deviceScaleFactor) * GCConstants.productImageViewHeight
}
