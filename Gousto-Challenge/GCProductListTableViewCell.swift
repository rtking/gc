//
//  GCProductListTableViewCell.swift
//  Gousto-Challenge
//
//  Created by Ryan on 12/04/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import UIKit

class GCProductListTableViewCell : UITableViewCell {
    // Outlets
    @IBOutlet var productImageView : UIImageView!
    @IBOutlet var productTitleLabel : UILabel!
    @IBOutlet var productPriceLabel : UILabel!
    
    // Setup function
    func setupCell(product : GCProduct) {
        self.productTitleLabel.text = product.title
        self.productPriceLabel.text = "£\(product.price!)"
        
        // Set the image correctly with image string
        if let imageString = product.imageString {
            self.productImageView.imageForURL(link: imageString)
        }
    }
    
    // Awake from Nib is only called once, so better to clip corners here
    override func awakeFromNib() {
        // Create a circular image shape by clipping bounds of image view
        self.productImageView.layer.cornerRadius = CGFloat(GCConstants.productImageViewHeight/2)
    }
}
