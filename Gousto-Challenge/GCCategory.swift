//
//  GCCategory.swift
//  Gousto-Challenge
//
//  Created by Ryan on 12/04/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import Foundation
import UIKit

class GCCategory : NSObject, NSCoding {
    // Variables
    var id : String!
    var title : String!
    var hidden : Bool!
    var filter : Bool!
    
    // MARK: INIT METHODS
    public init?(json: [String: Any]) {
        
        let id = json["id"] as? String
        let title = json["title"] as? String
        let hidden = json["hidden"] as? Bool
        let filter = false
        
        // Return nil if we do not have all attributes
        if (id != nil && title != nil && hidden != nil) {
            self.id = id
            self.title = title
            self.hidden = hidden
            self.filter = filter
        } else { return nil }
    }
    
    public init?(id : String?, title : String?, hidden : Bool?, filter : Bool?) {
        if(id != nil && title != nil && hidden != nil && filter != nil) {
            self.id = id
            self.title = title
            self.hidden = hidden
            self.filter = filter
        } else { return nil }
    }
    
    // MARK: MODEL METHODS
    
    static func filterOnlyAvailableCategories(categeryArray : [GCCategory]) -> [GCCategory] {
        return categeryArray.filter({$0.hidden == false})
    }
    
    // MARK: NSCODER METHODS
    
    required init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? String
        self.title = aDecoder.decodeObject(forKey: "title") as? String
        self.hidden = aDecoder.decodeObject(forKey: "hidden") as? Bool
        self.filter = aDecoder.decodeObject(forKey: "filter") as? Bool
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.hidden, forKey: "hidden")
        aCoder.encode(self.filter, forKey: "filter")
    }
}
