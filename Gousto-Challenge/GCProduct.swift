//
//  GCProduct.swift
//  Gousto-Challenge
//
//  Created by Ryan on 12/04/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import Foundation
import UIKit

class GCProduct : NSObject, NSCoding {
    // Variables
    var id : String!
    var title : String!
    var price : String!
    var descript : String!
    var isForSale : Bool!
    var boxLimit : String!
    var imageString : String?
    var categories : [GCCategory]!
    
    // MARK: INIT METHODS
    public init?(json: [String : Any]) {
        
        // Set the basic parameters
        let id = json["id"] as? String
        let title = json["title"] as? String
        let price = json["list_price"] as? String
        let description = json["description"] as? String
        let isForSale = json["is_for_sale"] as? Bool
        let boxLimit = json["box_limit"] as? String

        // Drill down to the image
        var imageSrc : String?
        if let imagesDict = json["images"] as? [String : Any] {
            if let imageSize = imagesDict.first?.key {
                if let imageSizeDict = imagesDict[imageSize] as? [String : Any]  {
                    imageSrc = imageSizeDict["src"] as? String
                }
            }
        }
        
        // Drill down to the categories
        var categoryArray = [GCCategory]()
        if let categoriesDict = json["categories"] as? [[String : Any]] {
            categoriesDict.forEach({ (object) in
                if let category = GCCategory(json: object) {
                    categoryArray.append(category)
                }
            })
        }
        
        // Return nil if we do not have all attributes
        if (id != nil && title != nil && price != nil && description != nil && imageSrc != nil && isForSale != nil && boxLimit != nil) {
            self.id = id
            self.title = title
            self.price = price
            self.descript = description
            self.isForSale = isForSale
            self.boxLimit = boxLimit
            self.imageString = imageSrc
            self.categories = categoryArray
        } else { return nil }
    }
    
    // MARK: MODEL METHODS
    
    // Function filters out products that have not been enabled by backend to be shown
    static func filterOnlyAvailableProducts(productArray : [GCProduct]) -> [GCProduct] {
        return productArray.filter({$0.isForSale == true})
    }
    
    // Function filters out products that have not been selected in the filter page
    static func filterOutFilteredProducts(productArray : [GCProduct], categoryArray : [GCCategory]) -> [GCProduct] {
        var newProductArray = [GCProduct]()
        // Filter out categories that we do not want
        let filteredCategories = categoryArray.filter({$0.filter == false})
        
        // Messy nested loops, getting all categories in products and seeing
        // if they are contained in the filtered categories
        productArray.forEach { (product) in
            categoryLoop: for category in product.categories {
                            for filteredCategory in filteredCategories {
                                if category.id == filteredCategory.id {
                                    newProductArray.append(product)
                                    break categoryLoop
                                }
                            }
            }
        }
        
        return newProductArray
    }
    
    // MARK: NSCODER METHODS
    
    required init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? String
        self.title = aDecoder.decodeObject(forKey: "title") as? String
        self.price = aDecoder.decodeObject(forKey: "price") as? String
        self.descript = aDecoder.decodeObject(forKey: "descript") as? String
        self.isForSale = aDecoder.decodeObject(forKey: "isForSale") as? Bool
        self.boxLimit = aDecoder.decodeObject(forKey: "boxLimit") as? String
        self.imageString = aDecoder.decodeObject(forKey: "imageString") as? String
        self.categories = aDecoder.decodeObject(forKey: "categories") as? [GCCategory]
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.price, forKey: "price")
        aCoder.encode(self.descript, forKey: "descript")
        aCoder.encode(self.isForSale, forKey: "isForSale")
        aCoder.encode(self.boxLimit, forKey: "boxLimit")
        aCoder.encode(self.imageString, forKey: "imageString")
        aCoder.encode(self.categories, forKey: "categories")
    }
}
