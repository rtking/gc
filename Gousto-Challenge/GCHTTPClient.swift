//
//  GCHTTPClient.swift
//  Gousto-Challenge
//
//  Created by Ryan on 12/04/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import Foundation

class GCHTTPClient {
    
    // Function gets all available categories from API endpoint
    static func getAllCategories(completionHandler: @escaping (Data?) -> Swift.Void) {
        // Create the URL
        if let categoryURL = URL(string: GCConstants.categoryAPIEndpoint) {
            // Make the network call
            let task = URLSession.shared.dataTask(with: categoryURL as URL) {
                data, response, error in
                // Make sure we have no errors
                guard error == nil else {
                    print(error!)
                    return
                }
                // Make sure we have data
                guard let data = data else {
                    print("Data is empty")
                    return
                }
                // Fire completionHandler
                completionHandler(data)
            }
            task.resume()
        }
    }
    
    // Function gets all available products, categories and images from API endpoint
    static func getAllProductsWithCategoriesAndImagesIncluded(imageSize : Double, completionHandler: @escaping (Data?) -> Swift.Void) {
        // Create the URL
        if let productURL = URL(string: "\(GCConstants.productAPIEndpoint)?includes[]=categories&image_sizes[]=\(imageSize)") {
            // Make the network call
            let task = URLSession.shared.dataTask(with: productURL as URL) {
                data, response, error in
                // Make sure we have no errors
                guard error == nil else {
                    print(error!)
                    return
                }
                // Make sure we have data
                guard let data = data else {
                    print("Data is empty")
                    return
                }
                // Fire completionHandler
                completionHandler(data)
            }
            task.resume()
        }
    }

    // Function retrieves all information about a particular product from API endpoint
    static func getProductForID(id : String, imageSize : Double, completionHandler: @escaping (Data?) -> Swift.Void) {
        // Create the URL
        if let productURL = URL(string: "\(GCConstants.productAPIEndpoint)/\(id)?includes[]=categories&image_sizes[]=\(imageSize)") {
            // Make the network call
            let task = URLSession.shared.dataTask(with: productURL as URL) {
                data, response, error in
                // Make sure we have no errors
                guard error == nil else {
                    print(error!)
                    return
                }
                // Make sure we have data
                guard let data = data else {
                    print("Data is empty")
                    return
                }
                // Fire completionHandler
                completionHandler(data)
            }
            task.resume()
        }
    }
}
