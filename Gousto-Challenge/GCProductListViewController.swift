//
//  GCProductsViewController.swift
//  Gousto-Challenge
//
//  Created by Ryan on 12/04/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import UIKit

class GCProductListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // Outlets
    @IBOutlet weak var productsTableView : UITableView!
    
    // Variables
    var categories : [GCCategory]!
    var products : [GCProduct]!
    
    // MARK: UIVIEW METHODS
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Init our main arrays
        self.categories = [GCCategory]()
        self.products = [GCProduct]()
        
        // Get our data to fill the main arrays
        self.getCategories()
        self.getProducts()
    }
    
    // MARK: DATA METHODS

    func getProducts() {
        // Check if we can retrieve the products first then retrieve from the server
        if let products = GCDatabaseHelper.retrieveProducts() {
            self.products = products
            self.products = GCProduct.filterOnlyAvailableProducts(productArray: self.products)
            self.products = GCProduct.filterOutFilteredProducts(productArray: self.products, categoryArray: self.categories)
            self.reloadMyTableView()
        }
        
        GCHTTPClient.getAllProductsWithCategoriesAndImagesIncluded(imageSize: GCConstants.expectedImageSize,
                                                                   completionHandler: { (data) in
            let jsonArray = GCJSONParser.getJSONArrayFromResponseData(jsonData: data)
            self.products = GCJSONParser.createProductArrayFromJSONArray(jsonArray: jsonArray)
            GCDatabaseHelper.saveProducts(newProductArray: self.products)
            self.products = GCProduct.filterOnlyAvailableProducts(productArray: self.products)
            self.products = GCProduct.filterOutFilteredProducts(productArray: self.products, categoryArray: self.categories)
            self.reloadMyTableView()
        })
    }
    
    func getCategories() {
        // Check if we can retrieve the categories first, otherwise retrieve from the server
        if let categories = GCDatabaseHelper.retrieveCategories() {
            self.categories = categories
            self.categories = GCCategory.filterOnlyAvailableCategories(categeryArray: self.categories)
            self.reloadMyTableView()
        } else {
            GCHTTPClient.getAllCategories { (data) in
                let jsonArray = GCJSONParser.getJSONArrayFromResponseData(jsonData: data)
                self.categories = GCJSONParser.createCategoryArrayFromJSONArray(jsonArray: jsonArray)
                GCDatabaseHelper.saveCategories(newCategoryArray: self.categories)
                self.categories = GCCategory.filterOnlyAvailableCategories(categeryArray: self.categories)
                self.reloadMyTableView()
            }
        }
    }
    
    // MARK: UITABLEVIEW METHODS
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(GCConstants.productTableViewCellHeight)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Instantiate the custom cell
        let cell : GCProductListTableViewCell = self.productsTableView.dequeueReusableCell(withIdentifier: "productcell") as! GCProductListTableViewCell
        
        // Get the product out of the array and pass it through to cell
        let product = self.products[indexPath.row]
        // Set up the cell
        cell.setupCell(product: product)
        
        return cell
    }
    
    // MARK: SEGUE METHODS
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailsViewController" ,
            let detailsVC = segue.destination as? GCDetailViewController ,
            let indexPath = self.productsTableView.indexPathForSelectedRow {
            let product = products[indexPath.row]
            detailsVC.product = product
        }
    }
    
    // MARK: HELPER METHODS
    
    // Function reloads the TableView safely in the main thread
    func reloadMyTableView() {
        // Reload our table in the main thread, as UIKit is not thread safe
        DispatchQueue.main.async {
            self.productsTableView.reloadData()
        }
    }
}
