//
//  GCFilterTableViewCell.swift
//  Gousto-Challenge
//
//  Created by Ryan on 12/04/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import UIKit

// Delegate which passes back touch feedback to the controller from the cell
protocol GCFilterCellDelegate {
    func filterChangedState(category : GCCategory, sender : UISwitch)
}

class GCFilterTableViewCell : UITableViewCell {
    // Outlets
    @IBOutlet var categoryLabel : UILabel!
    @IBOutlet var categorySwitch : UISwitch!
    
    // Variables
    var category : GCCategory!
    var delegate : GCFilterCellDelegate?
    
    // Setup function
    func setupCell() {
        categoryLabel.text = self.category.title
        categorySwitch.isOn = !self.category.filter
    }
    
    // Action gets called whenever the filter switch is pressed
    @IBAction private func filterPressed(sender: UISwitch) {
        delegate?.filterChangedState(category: self.category, sender: sender)
    }
}
